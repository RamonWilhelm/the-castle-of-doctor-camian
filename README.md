# The Castle of Doctor Camian

## Table of Contents  
- [Introduction](#introduction) 
- [Usage](#usage)
- [Features](#features)
- [Updates](#updates)
- [Future](#future)
- [Notes](#notes)

## Introduction

[![The Castle of Dr. Camian](http://i3.ytimg.com/vi/QmYpPCWIq2o/hqdefault.jpg)](https://www.youtube.com/watch?v=QmYpPCWIq2o&t=61s)

"You have been imprisoned in Dr. Camian's castle. He wants to turn you into a mindless zombie for his own army to conquer the entire world. 
Because you don't want to become his mindless slave you must find a way to escape from his castle... as soon as possible. 
Start the Castle of Dr. Camian AR App and hold your smart device on Room 001. Select one verb and start to play."

This was a final project for the Master's course [Cross-Media Design and Production](https://lnu.se/en/course/cross-media-design-and-production/distance-international-part-time-spring/) of the study program computer science at the [Linnaeus University](https://lnu.se/en/) in Växjö, Sweden. 
My idea is to create a small cross-media based adventure game, 
where the player immerse into the story by only playing with a web page, a smart device and AR markers. There are three markers, 
which represents a certain room of the game. The player has to visit this [**webpage**](https://castle-camian.web.app/), where he enters his name. 
After that the player uses his smart device to scan the room according to the instruction on the web page. Then the player has a verb GUI on the web page. 
On the smart device the player taps on Objects of the room to interact with them. The interaction depends on the verb which has been selected on the web page. 
Another benefit is the player can also take a closer look on the rooms with his smart device.

This game was developed with **Unity 2020.2.1f1 (64-Bit)**, **Vuforia** and **Googles FireBase**.
A demo video can be seen if you click on the screenshot above.

## Usage
* Print the three markers.
* Visit the webpage [**webpage**](https://castle-camian.web.app/) and enter your name.
* Start the AR app of The Castle of Dr. Camian.
* Scan the marker of the first room.
* Read the webpage for further instructions.
* Select your verb on the webpage and then tap on the 3D object on your smart device.

## Features
* A webpage with a score system, info display, verbs and inventory selection.
* Google Firebase repository which contains the webpage for the Firebase Hosting.
* Three markers for each room. 
* An AR app for your smart device.

## Updates
No Updates yet.

## Future
This AR game offers a great opportunity to revolutionize the adventure genre. Since this game was not well received in the evaluation by 20 participants because of the too tight coupling of the webpage,
I will completely renew this game. There should be much more markers, better 3D models and the inventory items and verbs should be selected directly in the app. The webpage should only serve as a bonus to see the score of the players.

## Notes
* In my evaluation with 20 participants, I found that the combination of AR app and website was not well received. It is better to keep the two separate. The website should only be used for not so important functions like the score system. 
* Make sure, you open the game with [**Unity 2020.2.1f1 (64-bit)**](https://unity3d.com/de/get-unity/download/archive).
* Marker Images are from [**Pixabay**](https://pixabay.com/).
* For more information about Google Firebase see the documentation [**here**](https://pixabay.com/).
* For more information about Vuforia see [**here**](https://developer.vuforia.com/).
* This project has been stored via [**Git LFS**](https://git-lfs.github.com/).
* The Google Firebase databank can be seen [here](https://console.firebase.google.com/u/0/project/castle-of-dr-camian-e5f1d/database/castle-of-dr-camian-e5f1d-default-rtdb/data).
* The Castle of Dr. Camian was developed for Android 8.0.