using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Firebase;
using Firebase.Database;


public class DungeonRoom : MonoBehaviour
{
    private string objName;
    private bool movedCrate;
    private bool lookPicture;
    private bool moveBars;

    public AudioClip movingCrate_sound;
    public AudioClip collectingScore_sound;
    public AudioClip won_sound;
    public AudioClip lose_sound;
    public AudioClip lever_sound;
    public AudioClip openBars_sound;
    public AudioClip openDoor_sound;
    public AudioClip key_jingle_sound;
    public AudioClip take_sound;
    public AudioClip unlock_sound;

    public AudioSource audio;

    public GameObject Room001;
    public GameObject Room002;
    public GameObject Room003;

    public GameObject Text001;
    public GameObject Text002;
    public GameObject Text003;

    public GameObject Bone;
    public GameObject Crate;
    public GameObject GreenSwitch;
    public GameObject Bars;
    public GameObject Key;
    public GameObject Hook;
    public GameObject Shield;
    public GameObject LeftBars;
    public GameObject RightBars;
    public GameObject FirstLever;
    public GameObject SecondLever;
    public GameObject ThirdLever;

    public static bool Room2Unlocked;
    public static bool Room3Unlocked;

    public Animator chesttop;
    public Animator openDoor;
    public Animator Lever001;
    public Animator Lever002;
    public Animator Lever003;

    private bool isUnlocked;
    private bool chestOpen;

    private DatabaseReference reference;
    private string verb;
    private string item;
    private Items items;
    private GoWest goWest;
    private bool state;
    private int score;
    private bool won;

    // Start is called before the first frame update
    void Start()
    {
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        items = new Items();
        movedCrate = false;
        goWest = new GoWest();
        isUnlocked = false;
        chestOpen = false;
        lookPicture = false;
        moveBars = false;

        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() 
    {
        FirebaseDatabase.DefaultInstance.GetReference("users/selVerb").GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                verb = snapshot.ToString(); 
            }
        });

        FirebaseDatabase.DefaultInstance.GetReference("users/selItem").GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                item = snapshot.ToString();
            }
        });

        FirebaseDatabase.DefaultInstance.GetReference("users/state").GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;


                state = (bool) snapshot.GetValue(true);
            
            }
        });

        FirebaseDatabase.DefaultInstance.GetReference("users/won").GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;


                won = (bool)snapshot.GetValue(true);

            }
        });

        FirebaseDatabase.DefaultInstance.GetReference("users/score").GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;

                score = (int) snapshot.GetValue(true);
            }
        });

        if (Input.GetMouseButtonDown(0) && !state && !won)
        {
            reference.Child("users/comment").SetValueAsync("Game Over!");
        }

        if (Input.GetMouseButtonDown(0) && !state && won)
        {
            reference.Child("users/comment").SetValueAsync("You have finished the game!");
        }


        //if (Input.GetMouseButtonDown(0) && state)
        if ((Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began && state))
            {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                objName = hit.transform.name;

                switch (objName)
                {
                    case "Chest":

                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("You wonder why this big chest is in the dungeon. What might be inside?");
                        }
                        else if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("It is too heavy to carry with you. Even if you could carry it, how would that heavy chest fit into your bag?");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("You try to move the heavy chest from its spot. After you realized it is too heavy for you you decided to stop.");
                        }
                        else if (verb.Contains("Open"))
                        {
                            if (!isUnlocked)
                            {
                                reference.Child("users/comment").SetValueAsync("It is locked. You are not able to open it.");
                            }
                            else
                            {
                                if (!chestOpen)
                                {
                                    audio.clip = openDoor_sound;
                                    audio.Play();
                                    chesttop.SetBool("IsUnlocked", true);
                                    chestOpen = true;
                                    reference.Child("users/comment").SetValueAsync("There was a massive shield inside. You take it with you.");
                                    items.AddItem(3, "Shield");
                                    string json = JsonUtility.ToJson(items);
                                    reference.Child("users/items").SetRawJsonValueAsync(json);
                                    score += 1;
                                    StartCoroutine(PlayScoreSoundDelay(collectingScore_sound, 1f));
                                    reference.Child("users").Child("score").SetValueAsync(score);
                                }
                                else
                                {
                                    reference.Child("users/comment").SetValueAsync("It is already open.");
                                }
                            }
                        }
                        else if (verb.Contains("Close"))
                        {
                            if (!chestOpen)
                            {
                                reference.Child("users/comment").SetValueAsync("It is already closed. If that is not good news.");
                            }
                            else
                            {
                                reference.Child("users/comment").SetValueAsync("After all you are lucky that the chest is finally open.");
                            }
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("You try to pick lock the chest with the bone... and you have no success.");
                            }
                            else if (item.Contains("Key"))
                            {
                                if (!isUnlocked)
                                {
                                    audio.clip = unlock_sound;
                                    audio.Play();
                                    reference.Child("users/comment").SetValueAsync("Yes! The key fits in that lock of the chest.");
                                    score += 1;
                                    StartCoroutine(PlayScoreSoundDelay(collectingScore_sound, 1f));
                                    reference.Child("users").Child("score").SetValueAsync(score);
                                    isUnlocked = true;
                                }
                                else
                                {
                                    reference.Child("users/comment").SetValueAsync("The chest is already unlocked.");
                                }
                            }
                            else if (item.Contains("Shield"))
                            {
                                reference.Child("users/comment").SetValueAsync("There is no reason to put the shield back into the chest.");
                            }
                        }
                        break;
                    case "Picture":
                        if (verb.Contains("Look"))
                        {
                            if (!lookPicture)
                            {
                                score += 1;
                                audio.clip = collectingScore_sound;
                                audio.Play();
                                reference.Child("users").Child("score").SetValueAsync(score);
                                lookPicture = true;
                            }

                            reference.Child("users/comment").SetValueAsync("You ask yourself what this symbol could mean.");
                        }
                        else if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("It is attached on the wall. You do not have to take it either.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("You hold your hands on this picture and you try to move it as hard as possible... but then you give up.");
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("Unfortunately, you reject the idea that there could be a safe behind this picture.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("If you do not even know if you can open it, how can you close then?");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("This makes no sense.");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("This makes no sense.");
                            }
                            else if (item.Contains("Shield"))
                            {
                                reference.Child("users/comment").SetValueAsync("This makes no sense.");
                            }
                        }
                        break;
                    case "Bone":

                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("There lies an old, long and rotten human bone on the floor.");
                        }
                        else if (verb.Contains("Take"))
                        {
                            Object.Destroy(Bone);
                            items.AddItem(0, "-- Inventory --");
                            items.AddItem(1, "Bone");
                            string json = JsonUtility.ToJson(items);
                            //reference.Child("users/items").RemoveValueAsync();
                            reference.Child("users").Child("items").SetRawJsonValueAsync(json);
                            reference.Child("users/comment").SetValueAsync("Yuk! You put that nasty bone into your pocket.");
                            audio.clip = take_sound;
                            audio.Play();
                            score += 1;
                            StartCoroutine(PlayScoreSoundDelay(collectingScore_sound, 1f));
                            reference.Child("users").Child("score").SetValueAsync(score);
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("You poke on the bone a few times and waste valuable time of your life.");
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("You do not want to know what is inside of this rotten bone.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("I do not understand the sense of this.");
                        }
                        break;
                    case "Crate":

                        if (verb.Contains("Look"))
                        {
                            if (!movedCrate)
                            {
                                reference.Child("users/comment").SetValueAsync("This is a crate.");
                            }
                            else
                            {
                                reference.Child("users/comment").SetValueAsync("I moved the great on the switch.");
                            }
                        }
                        else if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("The grate does not fit into your pocket.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            if (!movedCrate)
                            {
                                audio.clip = movingCrate_sound;
                                audio.Play();
                                movedCrate = true;
                                Debug.Log("Crate");
                                Object.Destroy(GreenSwitch);
                                Object.Destroy(Bars);
                                StartCoroutine(PlayScoreSoundDelay(openBars_sound, 0.5f));
                                Crate.transform.position = new Vector3(Crate.transform.position.x - 0.15f,
                                                                       Crate.transform.position.y,
                                                                       Crate.transform.position.z);

                                reference.Child("users/comment").SetValueAsync("The door is free! You can enter the Torture Room.");
                                reference.Child("users/unlockRoom2").SetValueAsync(true);
                                Room002.SetActive(true);
                                Text002.SetActive(false);
                                score += 1;
                                StartCoroutine(PlayScoreSoundDelay(collectingScore_sound, 1f));
                                reference.Child("users").Child("score").SetValueAsync(score);
                            }
                            else
                            {
                                Debug.Log("The crate has already been moved.");
                            }
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("The grate feels empty so there is no need to break it open.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("It is already done.");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("You hit the bone against the crate. You are surprised that nothing happens.");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("This is not a chest.");
                            }
                            else if (item.Contains("Shield"))
                            {
                                reference.Child("users/comment").SetValueAsync("I do not understand the purpose of that.");
                            }
                        }
                        break;
                    case "Switch":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("It looks like a big green squared button.");
                        }
                        else if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("How? It is attached on the floor.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("You press the button on the floor and see, how the bars go back into the wall. After you released the button, the bars fall back on the ground.");
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("You are funny! How can you open a button? It is only supposed to be pushed.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("This is completely nonsense.");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("The bone is too weak for this button.");
                            }
                        }
                        break;
                    case "Bars":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("Long, rusty, old bars are blocking your way to the freedom.");
                        }
                        else if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("You are not even able to move one of these bars a little tiny inch.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("You rattle on these bars and scream \"Let me out of here!\". Because nobody can hear you, you immediately stop your foolish behaviour and become quiet again.");

                            if (!moveBars)
                            {
                                moveBars = true;
                                score += 1;
                                audio.clip = collectingScore_sound;
                                audio.Play();
                                reference.Child("users").Child("score").SetValueAsync(score);
                            }
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("With what? With your bare hands?");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("Do you want to block your way even more?");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("You hit the bone against the bars, and you hear some musical tunes. Because you are not a good musician, you stop that noise.");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("The bars do not have a lock to unlock them. Maybe there is another way to get rid of them.");
                            }
                        }
                        break;
                    case "Door":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("This is a very big solid wooden door.");
                        }
                        else if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("You cannot do this! And I mean you really cannot do it.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("You knock on the door... but nothing happens.");
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("The door is locked. What now?");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("Do not worry. The door is already closed.");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("This would be a good idea if the door was not too strong.");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("The door has no key lock.");
                            }
                            else if (item.Contains("Shield"))
                            {
                                reference.Child("users/comment").SetValueAsync("You ram the shield against the door, but you cannot break it open.");
                            }
                        }
                        break;
                    case "Hook":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("A closer look reveals that it is actually a sharp hook on the wall. It looks like you could hang something heavy on it.");
                        }
                        else if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("Because of your fear of injuring your hands, you decide not to pull on this hook.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("You can hardly move this hook.");
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("You cannot do that.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("I am sorry. I do not understand, what you want.");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("Why should you hang the bone on the hook?");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("You are funny. Keep the key.");
                            }
                            else if (item.Contains("Shield"))
                            {
                                audio.clip = lever_sound;
                                audio.Play();
                                Shield.SetActive(true);
                                Hook.GetComponent<BoxCollider>().enabled = false;
                                StartCoroutine(PlayScoreSoundDelay(openDoor_sound, 0.2f));
                                openDoor.SetBool("OpenDoor", true);
                                items.RemoveItem(3);
                                string json = JsonUtility.ToJson(items);
                                reference.Child("users/items").SetRawJsonValueAsync(json);
                                reference.Child("users/comment").SetValueAsync("You hang the shield on the hook... and suddenly the door opens. You can now enter the prison.");
                                Room003.SetActive(true);
                                Text003.SetActive(false);
                                score += 1;
                                StartCoroutine(PlayScoreSoundDelay(collectingScore_sound, 1f));
                                reference.Child("users").Child("score").SetValueAsync(score);
                                reference.Child("users/unlockRoom3").SetValueAsync(true);
                            }
                        }
                        break;
                    case "Shield":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("The shield fits there well.");
                        }
                        else if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("You do not want to carry it with you again. It was a torture to bear this heavy shield around.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("You can hardly move this shield.");
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("You cannot do that.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("I am sorry. I do not understand, what you want.");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("No time to fool around.");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("No time to fool around.");
                            }
                        }
                        break;
                    case "Key":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("On a very high place on the wall hangs a rusty key.");
                        }
                        if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("This key is too high for you to reach it.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("This key is too high for you to reach it.");
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("You just joking, right?");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("You just joking, right?");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                audio.clip = key_jingle_sound;
                                audio.Play();
                                Object.Destroy(Key);
                                items.AddItem(2, "Key");
                                string json = JsonUtility.ToJson(items);
                                reference.Child("users/items").SetRawJsonValueAsync(json);
                                reference.Child("users/comment").SetValueAsync("You stretch your arm holding the bone on it... you poke gently into the key ring and... you finally got the key.");
                                score += 1;
                                StartCoroutine(PlayScoreSoundDelay(collectingScore_sound, 1f));
                                reference.Child("users").Child("score").SetValueAsync(score);
                            }
                        }
                        break;
                    case "Zombie":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("\"Oh my godness!\" you think, after you looked behind these bars. You hope, this zombie will never catch you.");
                        }
                        if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("Are you sure you want to die?");
                        }
                        else if (verb.Contains("Move"))
                        {
                            reference.Child("users/comment").SetValueAsync("I do not think the zombie likes it... and especially not you!");
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("Do you want to do an autopsy on this zombie?");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("What?! I think I do not like your sense of humor.");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                items.RemoveItem(1);
                                string json = JsonUtility.ToJson(items);
                                reference.Child("users/items").SetRawJsonValueAsync(json);
                                reference.Child("users/comment").SetValueAsync("You stick the bone in... suddenly the zombie grabs the bone and gnaws on it. You are glad that you do not have to carry this disgusting bone around anymore.");
                                score += 1;
                                StartCoroutine(PlayScoreSoundDelay(collectingScore_sound, 1f));
                                reference.Child("users").Child("score").SetValueAsync(score);
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("You are lucky that these bars cannot be opened by this key. Otherwise this would be a stupid idea to free that zombie.");
                            }
                        }
                        break;
                    case "Lever001":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("There is a lever on the wall.");
                        }
                        if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("You can only move it.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            audio.clip = lever_sound;
                            audio.Play();
                            Lever001.SetBool("switched", true);
                            reference.Child("users/comment").SetValueAsync("You pull the first lever down from the left.");
                            StartCoroutine(ZombieEscapes());
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("I did not know that you can open a lever.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("How is that possible?");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("This is absurd.");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("This is absurd.");
                            }
                        }
                        break;
                    case "Lever002":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("There is a lever on the wall.");
                        }
                        if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("You can only move it.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            audio.clip = lever_sound;
                            audio.Play();
                            Lever002.SetBool("switched", true);
                            reference.Child("users/comment").SetValueAsync("You pull the middle lever down.");
                            StartCoroutine(ZombieEscapes());
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("I did not know that you can open a lever.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("How is that possible?");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("This is absurd.");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("This is absurd.");
                            }
                        }
                        break;
                    case "Lever003":
                        if (verb.Contains("Look"))
                        {
                            reference.Child("users/comment").SetValueAsync("There is a lever on the wall.");
                        }
                        if (verb.Contains("Take"))
                        {
                            reference.Child("users/comment").SetValueAsync("You can only move it.");
                        }
                        else if (verb.Contains("Move"))
                        {
                            audio.clip = lever_sound;
                            audio.Play();
                            Lever003.SetBool("switched", true);
                            reference.Child("users/comment").SetValueAsync("You pull the last lever down from the left.");
                            StartCoroutine(OpenExit());
                        }
                        else if (verb.Contains("Open"))
                        {
                            reference.Child("users/comment").SetValueAsync("I did not know that you can open a lever.");
                        }
                        else if (verb.Contains("Close"))
                        {
                            reference.Child("users/comment").SetValueAsync("How is that possible?");
                        }
                        else if (verb.Contains("Use Item"))
                        {
                            if (item.Contains("Bone"))
                            {
                                reference.Child("users/comment").SetValueAsync("This is absurd.");
                            }
                            else if (item.Contains("Key"))
                            {
                                reference.Child("users/comment").SetValueAsync("This is absurd.");
                            }
                        }
                        break;
                }
            }
        }
    }

    IEnumerator PlayScoreSoundDelay(AudioClip sound, float sec)
    {
        yield return new WaitForSeconds(sec);
        audio.clip = sound;
        audio.Play();
    }

    IEnumerator OpenExit()
    {
        yield return new WaitForSeconds(2);
        audio.clip = openBars_sound;
        audio.Play();
        Object.Destroy(RightBars);
        reference.Child("users/comment").SetValueAsync("Congratulations!<br>You opened the right door to freedom!<br>You completed the game!");
        score += 1;
        StartCoroutine(PlayScoreSoundDelay(won_sound, 1f));
        reference.Child("users").Child("score").SetValueAsync(score);
        reference.Child("users/state").SetValueAsync(false);
        reference.Child("users/won").SetValueAsync(true);
    }

    IEnumerator ZombieEscapes()
    {
        yield return new WaitForSeconds(2);
        audio.clip = openBars_sound;
        audio.Play();
        StartCoroutine(PlayScoreSoundDelay(lose_sound, 1f));
        Object.Destroy(LeftBars);
        reference.Child("users/state").SetValueAsync(false);
        reference.Child("users/comment").SetValueAsync("Oh no!<br>The zombie is free!<br>He is grabbing on you and bites you to death.<br>You are now dead.");
    }
}
