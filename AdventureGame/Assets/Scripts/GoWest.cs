using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoWest
{
    public bool goWest;

    public GoWest()
    {

    }

    public GoWest(bool state)
    {
        this.goWest = state;
    }

    public void SetState(bool state)
    {
        this.goWest = state;
    }

}
