using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items
{
    public string[] items = new string[4];

    public Items()
    {

    }

    public Items(int i, string item)
    {
        items[i] = item;
    }

    public void AddItem(int i, string item)
    {
        items[i] = item;
    }

    public void RemoveItem(int i)
    {
        items[i] = null;
    }

    public string[] GetList()
    {
        return items;
    }
}
